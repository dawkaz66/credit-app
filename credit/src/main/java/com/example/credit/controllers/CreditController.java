package com.example.credit.controllers;

import com.example.credit.dtos.RestObject;
import com.example.credit.services.CreditService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
public class CreditController {

    private final CreditService service;

    @PostMapping("/createCredit")
    public String createCredit(@RequestBody RestObject restObject) {
        return "Credit number: " + service.createCredit(restObject);
    }

    @GetMapping("/getCredits")
    public List<RestObject> getCredits() {
        return service.prepareCreditList();
    }
}
