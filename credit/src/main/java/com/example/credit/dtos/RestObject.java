package com.example.credit.dtos;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestObject {

    private ClientDTO client;
    private ProductDTO product;
    private CreditDTO credit;
}
