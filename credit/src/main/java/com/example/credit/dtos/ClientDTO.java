package com.example.credit.dtos;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientDTO {

    private String firstname;
    private String surname;
    private String pesel;
}
