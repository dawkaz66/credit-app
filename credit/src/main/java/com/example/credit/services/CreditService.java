package com.example.credit.services;

import com.example.credit.dtos.ClientDTO;
import com.example.credit.dtos.ProductDTO;
import com.example.credit.mappers.ClientMapper;
import com.example.credit.mappers.CreditMapper;
import com.example.credit.mappers.ProductMapper;
import com.example.credit.models.Client;
import com.example.credit.models.Credit;
import com.example.credit.models.Product;
import com.example.credit.dtos.RestObject;
import com.example.credit.repositories.CreditRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Optional.of;

@AllArgsConstructor
@Service
public class CreditService {

    public static final String CREATE_CLIENT = "createClient";
    public static final String CREATE_PRODUCT = "createProduct";
    public static final String GET_CLIENTS = "getClients";
    public static final String GET_PRODUCTS = "getProducts";
//    @Value("${services.urls.client}")
    public static final String CLIENT_URL = "http://client-app:8081/";
//    @Value("${services.urls.product}")
    public static final String PRODUCT_URL = "http://product-app:8082/";
    private final CreditRepository repository;
    private final RestTemplate restTemplate;
    private final CreditMapper creditMapper;
    private final ClientMapper clientMapper;
    private final ProductMapper productMapper;

    public int createCredit(RestObject restObject) {
        Credit credit = creditMapper.mapFromDTO(restObject.getCredit());
        int creditId = repository.save(credit).getId();
        createClient(restObject.getClient(), creditId);
        createProduct(restObject.getProduct(), creditId);
        return creditId;
    }

    public List<RestObject> prepareCreditList() {
        List<RestObject> objects = new ArrayList<>();
        List<Client> clients = getClients();
        List<Product> products = getProducts();
        List<Credit> credits = getCredits();
        for (Credit credit : credits) {
            objects.add(RestObject.builder()
                    .credit(creditMapper.mapForDTO(credit))
                    .product(productMapper.mapForDTO(findProduct(products, credit.getId())))
                    .client(clientMapper.mapForDTO(findClient(clients, credit.getId())))
                    .build());
        }
        return objects;
    }

    private Product findProduct(List<Product> products, int id) {
        return products.stream().filter(product -> id == product.getCreditId()).findAny().orElse(null);
    }

    private Client findClient(List<Client> clients, int id) {
        return clients.stream().filter(client -> id == client.getCreditId()).findAny().orElse(null);
    }

    private void createClient(ClientDTO clientDTO, int creditId) {
        restTemplate.postForEntity(CLIENT_URL + CREATE_CLIENT, clientMapper.mapFromDTO(clientDTO, creditId), Client.class);
    }

    private void createProduct(ProductDTO productDTO, int creditId) {
        restTemplate.postForEntity(PRODUCT_URL + CREATE_PRODUCT, productMapper.mapFromDTO(productDTO, creditId), Product.class);
    }

    private List<Client> getClients() {
        return of(restTemplate.getForEntity(CLIENT_URL + GET_CLIENTS, Client[].class))
                .map(HttpEntity::getBody).map(Arrays::asList).orElse(null);
    }

    private List<Product> getProducts() {
        return of(restTemplate.getForEntity(PRODUCT_URL + GET_PRODUCTS, Product[].class))
                .map(HttpEntity::getBody).map(Arrays::asList).orElse(null);
    }

    private List<Credit> getCredits() {
        List<Credit> credits = new ArrayList<>();
        repository.findAll().forEach(credits::add);
        return credits;
    }
}
