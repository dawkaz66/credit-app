package com.example.credit.mappers;

import com.example.credit.dtos.ClientDTO;
import com.example.credit.dtos.CreditDTO;
import com.example.credit.dtos.ProductDTO;
import com.example.credit.dtos.RestObject;
import org.springframework.stereotype.Service;

@Service
public class RestObjectMapper {

    public CreditDTO mapCreditDTO(RestObject restObject) {
        return CreditDTO.builder()
                .creditName(restObject.getCredit().getCreditName())
                .build();
    }

    public ClientDTO mapClientDTO(RestObject restObject) {
        return ClientDTO.builder()
                .firstname(restObject.getClient().getFirstname())
                .surname(restObject.getClient().getSurname())
                .pesel(restObject.getClient().getPesel())
                .build();
    }

    public ProductDTO mapProductDTO(RestObject restObject) {
        return ProductDTO.builder()
                .productName(restObject.getProduct().getProductName())
                .value(restObject.getProduct().getValue())
                .build();
    }

    public RestObject mapRestObject(ClientDTO clientDTO, ProductDTO productDTO, CreditDTO creditDTO) {
        return RestObject.builder()
                .client(clientDTO)
                .product(productDTO)
                .credit(creditDTO)
                .build();
    }
}
