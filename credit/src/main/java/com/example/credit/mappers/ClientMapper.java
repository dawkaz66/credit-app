package com.example.credit.mappers;

import com.example.credit.dtos.ClientDTO;
import com.example.credit.models.Client;
import org.springframework.stereotype.Service;

@Service
public class ClientMapper {

    public Client mapFromDTO(ClientDTO clientDTO, int creditId) {
        return Client.builder()
                .creditId(creditId)
                .firstname(clientDTO.getFirstname())
                .surname(clientDTO.getSurname())
                .pesel(clientDTO.getPesel())
                .build();
    }

    public ClientDTO mapForDTO(Client client) {
        return ClientDTO.builder()
                .firstname(client.getFirstname())
                .surname(client.getSurname())
                .pesel(client.getPesel())
                .build();
    }
}
