package com.example.credit.mappers;

import com.example.credit.dtos.ProductDTO;
import com.example.credit.models.Product;
import org.springframework.stereotype.Service;

@Service
public class ProductMapper {

    public Product mapFromDTO(ProductDTO productDTO, int creditId){
        return Product.builder()
                .creditId(creditId)
                .productName(productDTO.getProductName())
                .value(productDTO.getValue())
                .build();
    }

    public ProductDTO mapForDTO(Product product) {
        return ProductDTO.builder()
                .productName(product.getProductName())
                .value(product.getValue())
                .build();
    }
}
