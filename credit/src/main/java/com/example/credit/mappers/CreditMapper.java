package com.example.credit.mappers;

import com.example.credit.dtos.CreditDTO;
import com.example.credit.models.Credit;
import org.springframework.stereotype.Service;

@Service
public class CreditMapper {

    public Credit mapFromDTO(CreditDTO creditDTO) {
        return Credit.builder()
//                .id(creditId)
                .creditName(creditDTO.getCreditName())
                .build();
    }

    public CreditDTO mapForDTO(Credit credit) {
        return CreditDTO.builder()
                .creditName(credit.getCreditName())
                .build();
    }
}
