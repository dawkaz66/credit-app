package com.example.credit.models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Client {

    private int creditId;
    private String firstname;
    private String surname;
    private String pesel;
}
