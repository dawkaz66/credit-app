package com.example.credit.models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {

    private int creditId;
    private String productName;
    private int value;
}
