package com.example.credit;

import com.example.credit.dtos.ClientDTO;
import com.example.credit.dtos.CreditDTO;
import com.example.credit.dtos.ProductDTO;
import com.example.credit.dtos.RestObject;
import com.example.credit.models.Client;
import com.example.credit.models.Credit;
import com.example.credit.models.Product;

public class BaseData {

    public RestObject prepareRestObject() {
        return RestObject.builder()
                .client(prepareClientDTO())
                .product(prepareProductDTO())
                .credit(prepareCreditDTO())
                .build();
    }

    public CreditDTO prepareCreditDTO() {
        return CreditDTO.builder()
                .creditName("creditDTO name")
                .build();
    }

    public ProductDTO prepareProductDTO() {
        return ProductDTO.builder()
                .productName("productDTO name")
                .value(1000)
                .build();
    }

    public ClientDTO prepareClientDTO() {
        return ClientDTO.builder()
                .firstname("clientDTO first name")
                .surname("clientDTO surname")
                .pesel("pesel")
                .build();
    }

    public Credit prepareCredit() {
        return Credit.builder()
                .id(1)
                .creditName("credit name")
                .build();
    }

    public Client prepareClient() {
        return Client.builder()
                .creditId(1)
                .firstname("client first name")
                .surname("client surname")
                .pesel("client pesel")
                .build();
    }

    public Product prepareProduct() {
        return Product.builder()
                .creditId(1)
                .value(10000)
                .productName("product name")
                .build();
    }

}
