package com.example.credit.services;

import com.example.credit.BaseData;
import com.example.credit.dtos.CreditDTO;
import com.example.credit.dtos.RestObject;
import com.example.credit.mappers.ClientMapper;
import com.example.credit.mappers.CreditMapper;
import com.example.credit.mappers.ProductMapper;
import com.example.credit.models.Client;
import com.example.credit.models.Credit;
import com.example.credit.models.Product;
import com.example.credit.repositories.CreditRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
class CreditServiceTest {

//    private BaseData base;
//    private CreditRepository repository;
//    private RestTemplate restTemplate;
//    private CreditMapper creditMapper;
//    private ClientMapper clientMapper;
//    private ProductMapper productMapper;
//    private CreditService service;
//
//    @BeforeEach
//    public void setup() {
//        base = new BaseData();
//        repository = Mockito.mock(CreditRepository.class);
//        restTemplate = Mockito.mock(RestTemplate.class);
//        creditMapper = Mockito.mock(CreditMapper.class);
//        clientMapper = Mockito.mock(ClientMapper.class);
//        productMapper = Mockito.mock(ProductMapper.class);
//        service = new CreditService(repository, restTemplate, creditMapper, clientMapper, productMapper);
//    }
//
//    @Test
//    void shouldCallMapMethod() {
//        //given
//        RestObject restObject = base.prepareRestObject();
//        Credit credit = base.prepareCredit();
//        CreditDTO creditDTO = base.prepareCreditDTO();
//        int id = 1;
//
//        //when
//        when(creditMapper.mapFromDTO(restObject.getCredit())).thenReturn(credit);
//        when(repository.save(credit)).thenReturn(credit);
//        spy(service.createCredit(restObject));
//        when(restTemplate.postForEntity("",base.prepareClient(), Client.class)).thenReturn(new ResponseEntity<>(HttpStatus.ACCEPTED));
//        when(restTemplate.postForEntity("",base.prepareClient(), Product.class)).thenReturn(new ResponseEntity<>(HttpStatus.ACCEPTED));
//
//        //then
//        verify(creditMapper).mapFromDTO(restObject.getCredit());
//    }
}
