package com.example.product.controllers;

import com.example.product.models.Product;
import com.example.product.services.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
public class ProductController {

    private final ProductService service;

    @GetMapping("/getProducts")
    public List<Product> getProduct() {
        return service.getProduct();
    }

    @PostMapping("/createProduct")
    public void createProduct(@RequestBody Product product) {
        service.createProduct(product);
    }
}
