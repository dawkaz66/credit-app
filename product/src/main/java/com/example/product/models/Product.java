package com.example.product.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@Entity
public class Product {

    @Id
    private int creditId;
    private String productName;
    private int value;
}
