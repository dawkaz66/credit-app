package com.example.client.services;

import com.example.client.models.Client;
import com.example.client.repositories.ClientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class ClientService {

    private final ClientRepository repository;

    public void createClient(Client client) {
        repository.save(client);
    }

    public List<Client> getClients() {
        List<Client> clients = new ArrayList<>();
        repository.findAll().forEach(clients::add);
        return clients;
    }
}
