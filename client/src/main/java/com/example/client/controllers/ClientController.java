package com.example.client.controllers;

import com.example.client.models.Client;
import com.example.client.services.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
public class ClientController {

    private final ClientService service;

    @GetMapping("/getClients")
    public List<Client> getClient() {
        return service.getClients();
    }

    @PostMapping("/createClient")
    public void createClient(@RequestBody Client client) {
        service.createClient(client);
    }

}
