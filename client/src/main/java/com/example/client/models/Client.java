package com.example.client.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@Entity
public class Client {

    @Id
    private int creditId;
    private String firstname;
    private String surname;
    private String pesel;
}
